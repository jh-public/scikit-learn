#scikit-learn 0.24.1 with XGBoost, LightGBM, CatBoost
==============
- python            3.9.2
- iPython           7.21.0
- scikit-learn      0.24.1
- scipy             1.6.1
- pandas            0.24.2
- numpy             1.20.1
- matplotlib        3.3.4
- xgboost           1.3.3
- LightGBM          3.1.1
- CatBoost          0.24.4
- Seaborn           0.11.1
- jupyter-notebook  6.2.0

Usage
---------
    docker run -it \
        -v $PWD:/home/user/$(basename $PWD) \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -e DISPLAY=${DISPLAY:-:0} \
        huanjason/scikit-learn


To run Jupyter:

    docker run -it \
        -v $PWD:/home/user/$(basename $PWD) \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -e DISPLAY=${DISPLAY:-:0} \
        -p 8888:8888 \
        -h localhost \
        huanjason/scikit-learn \
        jupyter notebook --ip=0.0.0.0
