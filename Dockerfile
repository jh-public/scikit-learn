FROM python:3.9

LABEL maintainer="huan.jason@gmail.com"

RUN \
    useradd user \
    && mkdir /home/user \
    && chown user:user /home/user \
    && pip3 install \
        ipython \
        scipy \
        scikit-learn \
        pandas \
        matplotlib \
        xgboost \
        lightgbm \
        catboost \
        seaborn \
        jupyter \
        jupyterlab

EXPOSE 8888

WORKDIR /home/user

USER user

CMD [ "ipython" ]
